from django.urls import path, re_path

from .views import *

urlpatterns = [
    path('', PieceListView.as_view(), name='pieces-list'),
    path('piece/new', PieceCreateView.as_view(), name='piece-create'),
    path('piece/<int:pk>/', PieceDetailView.as_view(), name='piece-view'),
    re_path(r'^piece/(?P<pk>\d+)/update', PieceUpdateView.as_view(), name='piece-update'),
    path('piece/<int:pk>/delete', PieceDeleteView.as_view(), name='piece-delete'),
    path('loan/', LoanListView.as_view(), name='loan-list'),
    path('loan/new', LoanCreateView.as_view(), name='loan-create'),
    path('loan/<int:pk>/', LoanDetailView.as_view(), name='loan-view'),
    path('loan/<int:pk>/update', LoanUpdateView.as_view(), name='loan-update'),
    path('loan/<int:pk>/delete', LoanDeleteView.as_view(), name='loan-delete'),
    path('loan/<int:pk>/back', LoanBackView.as_view(), name='loan-back'),
]
