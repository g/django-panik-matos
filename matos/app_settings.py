import os

from django.conf import settings


class AppSettings:
    def get_setting(self, setting, default=None):
        return getattr(settings, 'MATOS_' + setting, default)

    @property
    def LATENESS_REMINDERS(self):
        return self.get_setting('LATENESS_REMINDERS', default=False)

    @property
    def LATENESS_REMINDERS_REPLY_TO(self):
        return self.get_setting('LATENESS_REMINDERS_REPLY_TO', default=settings.DEFAULT_FROM_EMAIL)

    @property
    def LATENESS_REMINDERS_DELAYS(self):
        return self.get_setting('LATENESS_REMINDERS_DELAYS', default=(3, 10))

    @property
    def INSURANCE_CATEGORIES(self):
        return self.get_setting('INSURANCE_CATEGORIES', default=())


app_settings = AppSettings()
