from django import forms
from django.forms.widgets import SelectMultiple, TextInput
from django.utils.translation import gettext_lazy as _

from .app_settings import app_settings
from .models import Loan, Piece


class BasketItemsWidget(SelectMultiple):
    template_name = 'matos/basket-items.html'


class LoanerWidget(TextInput):
    template_name = 'matos/loaner-input.html'


class LoanForm(forms.ModelForm):
    class Meta:
        model = Loan
        exclude = ['back_timestamp']
        labels = {
            'items': _('Selected items'),
        }
        widgets = {
            'items': BasketItemsWidget(),
            'loaner': LoanerWidget(),
            'date_start': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
            'date_end': forms.DateInput(attrs={'type': 'date'}, format='%Y-%m-%d'),
        }


class PieceForm(forms.ModelForm):
    class Meta:
        model = Piece
        fields = '__all__'

    insurance_category = forms.ChoiceField(
        label=_('Insurance category'), choices=[('', '')] + list(app_settings.INSURANCE_CATEGORIES)
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not app_settings.INSURANCE_CATEGORIES:
            self.fields['insurance_category'].widget = forms.HiddenInput()
