from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ('matos', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Loan',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('date_start', models.DateField(null=True)),
                ('date_end', models.DateField(null=True)),
                ('comment', models.TextField(blank=True)),
                ('loaner', models.CharField(max_length=50, verbose_name='Loaner', blank=True)),
            ],
            options={
                'verbose_name': 'Loan',
                'verbose_name_plural': 'Loans',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LoanPiece',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('quantity', models.IntegerField(default=1, verbose_name='Quantity')),
                ('loan', models.ForeignKey(to='matos.Loan', on_delete=models.CASCADE)),
                ('piece', models.ForeignKey(to='matos.Piece', on_delete=models.CASCADE)),
            ],
            options={},
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='loan',
            name='pieces',
            field=models.ManyToManyField(
                to='matos.Piece', null=True, verbose_name='Pieces', through='matos.LoanPiece', blank=True
            ),
            preserve_default=True,
        ),
    ]
