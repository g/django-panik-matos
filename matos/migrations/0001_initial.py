from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = []

    operations = [
        migrations.CreateModel(
            name='Piece',
            fields=[
                (
                    'id',
                    models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True),
                ),
                ('title', models.CharField(max_length=50, verbose_name='Title')),
                ('reference', models.CharField(max_length=50, verbose_name='Reference', blank=True)),
                ('comment', models.TextField(verbose_name='Comment', blank=True)),
                ('location', models.TextField(verbose_name='Location', blank=True)),
                (
                    'image',
                    models.ImageField(
                        max_length=250, upload_to=b'matos', null=True, verbose_name='Picture', blank=True
                    ),
                ),
                ('in_use', models.BooleanField(default=False, verbose_name='In use')),
                ('rentable', models.BooleanField(default=False, verbose_name='Rentable')),
                ('quantity', models.IntegerField(default=1, null=True, verbose_name='Quantity')),
                ('purchase_date', models.DateField(null=True, verbose_name='Purchase Date', blank=True)),
                (
                    'purchase_price',
                    models.DecimalField(
                        null=True, verbose_name='Purchase Price', max_digits=10, decimal_places=2, blank=True
                    ),
                ),
                ('creation_timestamp', models.DateTimeField(auto_now_add=True, null=True)),
                ('last_update_timestamp', models.DateTimeField(auto_now=True, null=True)),
            ],
            options={
                'ordering': ['title'],
                'verbose_name': 'Piece',
                'verbose_name_plural': 'Pieces',
            },
            bases=(models.Model,),
        ),
    ]
