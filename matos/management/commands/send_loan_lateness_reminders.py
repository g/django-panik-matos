import logging

from django.core.mail import EmailMessage
from django.core.management.base import BaseCommand
from django.template import loader
from django.utils.timezone import now

from matos.app_settings import app_settings
from matos.models import Loan


class Command(BaseCommand):
    def handle(self, verbosity, **kwargs):
        self.verbose = verbosity > 1

        if not app_settings.LATENESS_REMINDERS:
            if self.verbose:
                print('lateness reminders are disabled')
            return

        logger = logging.getLogger('panikdb')

        late_subject = loader.get_template('matos/lateness_reminder_subject.txt')
        late_body = loader.get_template('matos/lateness_reminder_email_body.txt')

        for loan in Loan.objects.filter(back_timestamp__isnull=True, date_end__lt=now()):
            if loan.lateness() in app_settings.LATENESS_REMINDERS_DELAYS:
                continue
            if not loan.loaner_email:
                continue

            context = {'loan': loan}
            if self.verbose:
                print('sending lateness reminder to %s' % loan.loaner_email)
            logger.info('sending lateness reminder to %s', loan.loaner_email)
            message = EmailMessage(
                late_subject.render(context).strip(),
                late_body.render(context).strip(),
                to=[loan.loaner_email],
                reply_to=[app_settings.LATENESS_REMINDERS_REPLY_TO],
            )
            message.send()
