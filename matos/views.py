import datetime

from django.contrib.auth import get_user_model
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.text import slugify
from django.utils.timezone import now
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView

from .forms import LoanForm, PieceForm
from .models import Loan, Location, Piece


class PiecePermissionViewMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('matos.add_piece'):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


class LoanPermissionViewMixin:
    def dispatch(self, request, *args, **kwargs):
        if not request.user.has_perm('matos.add_loan'):
            raise PermissionDenied()
        return super().dispatch(request, *args, **kwargs)


class PieceListView(ListView):
    model = Piece

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)
        queryset = queryset.select_related('location')
        if 'location' in self.request.GET:
            queryset = queryset.filter(location__id=self.request.GET['location'])
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        if 'location' in self.request.GET:
            context['location_id'] = self.request.GET['location']
        context['locations'] = Location.objects.all()
        context['rentable'] = [x for x in self.get_queryset() if x.rentable]
        context['stock'] = [x for x in self.get_queryset() if not x.in_use and not x.rentable]
        context['in_use'] = [x for x in self.get_queryset() if x.in_use]
        return context


class PieceDetailView(DetailView):
    model = Piece

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['latest_loans'] = self.object.loan_set.order_by('-date_end')[:10]
        return context


class PieceCreateView(PiecePermissionViewMixin, CreateView):
    model = Piece
    form_class = PieceForm


class PieceUpdateView(PiecePermissionViewMixin, UpdateView):
    model = Piece
    form_class = PieceForm


class PieceDeleteView(PiecePermissionViewMixin, DeleteView):
    model = Piece

    def get_success_url(self):
        return reverse('pieces-list')


class LoanFormContextMixin:
    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        current_loans = Loan.objects.filter(back_timestamp__isnull=True)
        if self.object and self.object.id:
            current_loans = current_loans.exclude(id=self.object.id)
        current_loans = current_loans.order_by('date_start').prefetch_related('items')
        context['pieces'] = list(Piece.objects.filter(rentable=True))
        for piece in context['pieces']:
            piece.current_loans = [x for x in current_loans if piece in x.items.all()]
        context['locations'] = Location.objects.filter(id__in=[x.location_id for x in context['pieces']])
        context['members'] = [
            {
                'name': x.get_full_name(),
                'slugged_name': slugify(x.get_full_name()),
                'email': x.email,
            }
            for x in get_user_model().objects.all()
        ]
        return context


class LoanListView(ListView):
    model = Loan

    def get_queryset(self, *args, **kwargs):
        queryset = super().get_queryset(*args, **kwargs)
        queryset = queryset.filter(back_timestamp__isnull=True).order_by('-date_end')
        return queryset

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['object_list'] = list(context['object_list'])
        context['object_list'].sort(key=lambda x: -x.lateness())
        return context


class LoanCreateView(LoanPermissionViewMixin, LoanFormContextMixin, CreateView):
    model = Loan
    form_class = LoanForm

    def get_initial(self):
        date_end = now().date() + datetime.timedelta(days=2)  # 2 days
        if date_end.weekday() in (5, 6):  # no return on weekends
            date_end += datetime.timedelta(days=7 - date_end.weekday())
        return {'date_start': now().date(), 'date_end': date_end}


class LoanDetailView(LoanPermissionViewMixin, DetailView):
    model = Loan

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        current_loans = Loan.objects.filter(back_timestamp__isnull=True)
        if self.object and self.object.id:
            current_loans = current_loans.exclude(id=self.object.id)
        current_loans = current_loans.order_by('date_start').prefetch_related('items')
        context['pieces'] = list(self.object.items.all())
        for piece in context['pieces']:
            piece.current_loans = [x for x in current_loans if piece in x.items.all()]
        return context


class LoanUpdateView(LoanPermissionViewMixin, LoanFormContextMixin, UpdateView):
    model = Loan
    form_class = LoanForm


class LoanDeleteView(LoanPermissionViewMixin, DeleteView):
    model = Loan

    def get_success_url(self):
        return reverse('loan-list')


class LoanBackView(LoanPermissionViewMixin, DetailView):
    model = Loan

    def get(self, request, **kwargs):
        obj = self.get_object()
        obj.back_timestamp = now()
        obj.save()
        return HttpResponseRedirect(reverse('loan-view', kwargs={'pk': obj.id}))
