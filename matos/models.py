from django.db import models
from django.urls import reverse
from django.utils.timezone import now
from django.utils.translation import gettext_lazy as _

from matos.app_settings import app_settings


class Location(models.Model):
    label = models.CharField(_('Location'), max_length=50)
    colour_code = models.CharField(_('Colour code'), max_length=20, default='#ffffff')

    class Meta:
        ordering = ['label']

    def __str__(self):
        return self.label


class Piece(models.Model):
    class Meta:
        verbose_name = _('Piece')
        verbose_name_plural = _('Pieces')
        ordering = ['title']

    title = models.CharField(_('Title'), max_length=50)
    reference = models.CharField(_('Reference'), max_length=50, blank=True)
    comment = models.TextField(_('Comment'), blank=True)
    location = models.ForeignKey(
        Location, verbose_name=_('Location'), null=True, blank=True, on_delete=models.SET_NULL
    )
    location_details = models.TextField(_('Location Details'), blank=True)
    image = models.ImageField(_('Picture'), upload_to='matos', max_length=250, null=True, blank=True)

    manual_file = models.FileField(_('Manual (file)'), upload_to='matos', null=True, blank=True)
    manual_url = models.URLField(_('Manual (URL)'), max_length=255, null=True, blank=True)

    in_use = models.BooleanField(_('In use'), default=False)
    rentable = models.BooleanField(_('Rentable'), default=False)
    quantity = models.IntegerField(_('Quantity'), default=1, null=True)

    purchase_date = models.DateField(_('Purchase Date'), null=True, blank=True)
    purchase_price = models.DecimalField(
        _('Purchase Price'), max_digits=10, decimal_places=2, null=True, blank=True
    )
    insurance_category = models.CharField(_('Insurance category'), max_length=20, blank=True, default='')

    creation_timestamp = models.DateTimeField(auto_now_add=True, null=True)
    last_update_timestamp = models.DateTimeField(auto_now=True, null=True)

    def __unicode__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('piece-view', kwargs={'pk': self.id})


class Loan(models.Model):
    class Meta:
        verbose_name = _('Loan')
        verbose_name_plural = _('Loans')

    date_start = models.DateField(_('Start date'), null=True)
    date_end = models.DateField(_('End date'), null=True)
    loaner = models.CharField(_('Loaner'), blank=True, max_length=50)
    loaner_email = models.EmailField(_('Loaner email'), blank=True)
    items = models.ManyToManyField(Piece, verbose_name=_('Items'), blank=True)
    comment = models.TextField(_('Comment'), blank=True)

    back_timestamp = models.DateTimeField(null=True)

    def get_absolute_url(self):
        return reverse('loan-view', kwargs={'pk': self.id})

    def lateness(self):
        if self.back_timestamp:
            lateness = (self.back_timestamp.date() - self.date_end).days
        else:
            lateness = (now().date() - self.date_end).days
        if lateness > 0:
            return lateness
        return 0
